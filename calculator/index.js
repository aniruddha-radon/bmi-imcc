// STEP 3:

// $ -> jquery function
// accepts a reference of a DOM element or the dom itself
// jquery understand that it needs to something with the element
// . operator ==> helps us do something/handle something
// ready ==> is a method that accepts a callback
// when the document (DOM) is ready call the callback function
// alternate syntax in the data-display index.js
// the callback function is an arrow function
$(document).ready(() => {
    // code below

    // select button reference from DOM and store it in heightPlus const
    // reference of the element in the DOM that has id height-plus
    // # refers to id
    // line 35 in the HTML
    const heightPlus = $("#height-plus");

    // adding an event listener to listen to click events
    // whenever there is a click on the heightPlus element call the incrementHeight function
    $(heightPlus).on('click', incrementHeight);

    // select button reference from DOM and attach event listener (short cut for above)
    // on each click of the the element with id height-minus call the the decrementHeight function
    $("#height-minus").on('click', decrementHeight);

    // $("#weight-plus") -> selects the element with id weight-plus
    // on -> registers an even on the element
    // on accepts 2 parameters
    // 1. event that you want to listen to on that element
    // 2. function to call when the even happens (is triggered)
    // on click of the an element with if of weight-plus we want to call incrementHeight
    $("#weight-plus").on('click', incrementWeight);


     // $("#weight-plus") -> selects the element with id weight-plus
    // on -> registers an even on the element
    // on accepts 2 parameters
    // 1. event that you want to listen to on that element
    // 2. function to call when the even happens (is triggered)
    $("#weight-minus").on('click', decrementWeight);

    // select element with id save
    // register an event for clicks
    // on each click call the saveBMI function
    $("#save").on("click", saveBMI);
});

// GLOBALS
let height = 5.0;
let weight = 50;
let bmi = 20;

// function that increments the height by 0.1 and puts in the height span
const incrementHeight = () => {
    // toFixed tells js how many decimal numbers we want
    height = parseFloat((height + 0.1).toFixed(1));
    // text method updates the value of the element
    // select an element with id height
    // update the text of the this element and set it to the height
    $("#height").text(height);

    // invoke/call/execute the calculateBMI function
    calculateBMI();
}

const decrementHeight = () => {
    height = parseFloat((height - 0.1).toFixed(1));
    $("#height").text(height);
    calculateBMI();
}

const incrementWeight = () => {
    weight = parseFloat((weight + 1).toFixed(1));
    $("#weight").text(weight);
    calculateBMI();
}

const decrementWeight = () => {
    weight = parseFloat((weight - 1).toFixed(1));
    $("#weight").text(weight);
    calculateBMI();
}

const calculateBMI = () => {

    let heightInMeters = height * 0.3048;
    bmi = weight / (heightInMeters ** 2);

    // select an element with the id score
    // update the text inside this element with
    // bmi fixed to 2 decimal places
    $("#score").text(bmi.toFixed(2));

    if (bmi > 29) {
        // select element with id description
        // and update its text to 'Obese'
        return $("#description").text("Obese");
    }

    if (bmi > 24) {
        return $("#description").text("Overweight");
    }

    if (bmi > 20) {
        return $("#description").text("Normal");
    }

    return $("#description").text("Underweight");

}

const saveBMI = async () => {
    try {
        await post({ height, weight, bmi }, 'bmi');
        alert('BMI POSTED');
    } catch(e) {
        alert('something went wrong');
    }
}

const getBMI = async _ => {
    try {
        const bmiData = await get('bmi');
        console.log(bmiData);
    } catch(e) {
        alert('something went wrong');
    }
}



// ALTERNATE SYNTAX FOR $(document).ready();
$(() => {
    console.log('this is the alternate syntax for $(document).ready() method');
})