
// fire the callback when the document (DOM) is completely ready
// alternate syntax (updated syntax) for $(document).ready(() => {});
$(() => {
    $("#update").on("click", getData)
})

const attachData = (bmiRows) => {
    const tableBody = $("#table-body");
    tableBody.html('');

    for(let { height, weight, bmi } of bmiRows) {
        tableBody.append(`
            <div>
                <span>${height}</span>
                <span>${weight}</span>
                <span>${bmi.toFixed(2)}</span>
            </div>
        `)
    }
}

const getData = async () => {
    try {
        const response = await get("bmi");
        attachData(response.data);
    } catch(e) {
        alert('something went wrong');
    }
}

getData();