const baseURL = 'http://localhost:3000'

const post = (data, endpoint) => {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `${baseURL}/${endpoint}`,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(data),
            success: (response) => resolve(response),
            error: (err) => reject(err)
        })
    })
}

const get = (endpoint) => {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `${baseURL}/${endpoint}`,
            method: 'GET',
            success: (response) => resolve(response),
            error: (err) => reject(err)
        })
    })
}